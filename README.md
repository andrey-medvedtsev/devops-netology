# devops-netology
Будут проигнорированы все вложенные каталоги terraform:
**/.terraform/*

Будут проигнорированы все файлы с расширением tfstate и содержащие в названии tfstate:
*.tfstate
*.tfstate.*

Будет проигнорирован файл:
crash.log

Будут проигнорированы все файлы с раширением tfvars:
*.tfvars

Будут проигнорированы файлы:
override.tf
override.tf.json

Будут проигнорированы все файлы содержащие в названии _override.tf и _override.tf.json:
*_override.tf
*_override.tf.json

Будут проигнорированы файлы:
.terraformrc
terraform.rc


